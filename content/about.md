+++
title = "About Me"
slug = "about"
+++

I'm Giorgio Marinato, PhD candidate in cognitive neuroscience at CIMeC - Center
for Mind/Brain Sciences, University of Trento, Italy.  
I work mainly with magnetoencephalography (MEG) doing research on auditory
attention and scene analysis using ecologically valid soundscapes.

Interested in open science practices and policies since many years.

##### MEG data analysis

* Brainsotorm + FieldTrip + Matlab custom code

##### Writing workflow

- Markdown in Vim + Zotero with Better BibTeX (BBT) + pandoc

##### Self training on

- Python
- Machine learning
- BIDS data structure
- DataLad / git-annex


Have questions or suggestions? Feel free to [open an issue on
GitLab](https://gitlab.com/Grg_M/neurogima.gitlab.io/-/issues/new) or [ask me on
Twitter](https://twitter.com/neurogima).
