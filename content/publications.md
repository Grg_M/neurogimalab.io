# Publications

- de Vries, I.E.J., **Marinato, G.**, Baldauf, D. (2021). 
  Decoding auditory object-based attention from source-reconstructed MEG alpha oscillation. 
  Journal of Neuroscience. DOI: https://doi.org/10.1523/JNEUROSCI.0583-21.2021

- Gau, R., Noble, S., Heuer, K., Bottenhorn, K. L., Bilgin, I. P., Yang, Y. F., ... & Marinazzo, D. (2021).
  Brainhack: Developing a culture of open, inclusive, community-driven neuroscience. 
  Neuron, 109(11), 1769-1775. DOI:https://doi.org/10.1016/j.neuron.2021.04.001 

- **Marinato, G.**, & Baldauf, D. (2019). Object-based attention in complex,
  naturalistic auditory streams. Scientific reports, 9(1), 1-13.  
  [Golden open access from publisher](https://www.nature.com/articles/s41598-019-39166-6)
